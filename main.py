import sys
import time
from functools import wraps
from typing import List, Optional, Type

import pandas as pd
from pydantic import AliasChoices, AliasPath, BaseModel, Field
from v4_client_py.clients import IndexerClient
from v4_client_py.clients.constants import IndexerConfig

INDEXER_REST_ENDPOINT = "https://indexer.dydx.trade"
INDEXER_WS_ENDPOINT = "wss://indexer.dydx.trade/v4/ws"
config = IndexerConfig(rest_endpoint=INDEXER_REST_ENDPOINT, websocket_endpoint=INDEXER_WS_ENDPOINT)

PUBADDRES = "dydx1y8spg83g2czq22q2gmptjyxv8r5ys9mx9n8qxc"
SUBACCOUNT = 0
LIMIT = 1000
FROM_ARGS = sys.argv[1]
TO_ARGS = sys.argv[2]
FROM_DATE = pd.to_datetime(FROM_ARGS).strftime("%Y-%m-%dT%H:%M:%S.%fZ")
TO_DATE = pd.to_datetime(TO_ARGS).strftime("%Y-%m-%dT%H:%M:%S.%fZ")

class Transfer(BaseModel):
    id: str
    sender_addr: str = Field(..., validation_alias=AliasPath("sender", "address"))
    receiver_addr: str = Field(..., validation_alias=AliasPath("recipient", "address"))
    amount: float = Field(..., validation_alias=AliasChoices("size"))
    symbol: str
    created_at: str = Field(..., validation_alias=AliasChoices("createdAt"))
    created_height: int = Field(..., validation_alias=AliasChoices("createdAtHeight"))
    tx_hash: str = Field(..., validation_alias=AliasChoices("transactionHash"))
    type: str


class Position(BaseModel):
    address: str = PUBADDRES
    market: str
    status: str
    side: str
    size: float
    maxSize: float
    entryPrice: float
    exitPrice: Optional[float]
    realizedPnl: float
    unrealizedPnl: float
    created_at: str = Field(..., validation_alias=AliasChoices("createdAt"))
    createdAtHeight: int
    closedAt: Optional[str]
    sumOpen: float
    sumClose: float
    netFunding: float
    subaccountNumber: int


class Order(BaseModel):
    address: str = PUBADDRES
    id: str
    pair_id: str = Field(..., alias="clobPairId")
    side: str
    size: float
    total_filled: float = Field(..., alias="totalFilled")
    price: float
    type: str
    status: str
    time_in_force: str = Field(..., alias="timeInForce")
    reduce_only: bool = Field(..., alias="reduceOnly")
    order_flags: str = Field(..., alias="orderFlags")
    good_til_block: str = Field(..., alias="goodTilBlock")
    created_at_height: int = Field(..., alias="createdAtHeight")
    client_metadata: str = Field(..., alias="clientMetadata")
    updated_at: str = Field(..., alias="updatedAt", serialization_alias="created_at")
    updated_at_height: int = Field(..., alias="updatedAtHeight")
    post_only: bool = Field(..., alias="postOnly")
    ticker: str
    subaccount_number: int = Field(..., alias="subaccountNumber")


class Trade(BaseModel):
    address: str = PUBADDRES
    id: str
    side: str
    liquidity: str
    trade_type: str = Field(..., alias="type")
    market: str
    market_type: str = Field(..., alias="marketType")
    price: float
    size: float
    fee: float
    created_at: str = Field(..., alias="createdAt")
    created_at_height: int = Field(..., alias="createdAtHeight")
    order_id: str = Field(..., alias="orderId")
    client_metadata: str = Field(..., alias="clientMetadata")
    subaccount_number: int = Field(..., alias="subaccountNumber")


class account(BaseModel):
    order: List[Order]
    transfer: List[Transfer]
    position: List[Position]
    trade: List[Trade]

    def get_order_data(self) -> list[dict]:
        return [order.model_dump() for order in self.order]

    def get_transfer_data(self) -> list[dict]:
        return [transfer.model_dump() for transfer in self.transfer]

    def get_position_data(self) -> list[dict]:
        return [position.model_dump() for position in self.position]

    def get_trade_data(self) -> list[dict]:
        return [trade.model_dump() for trade in self.trade]
    
    def create_csv(self):
        order_df = pd.DataFrame(self.get_order_data())
        order_df.drop_duplicates(subset=["id"], inplace=True)
        order_df.to_csv("order.csv", index=False)
        transfer_df = pd.DataFrame(self.get_transfer_data())
        transfer_df.drop_duplicates(subset=["id"], inplace=True)
        transfer_df.to_csv("transfer.csv", index=False)
        position_df = pd.DataFrame(self.get_position_data())
        position_df.to_csv("position.csv", index=False)
        trade_df = pd.DataFrame(self.get_trade_data())
        trade_df.drop_duplicates(subset=["id"], inplace=True)
        trade_df.to_csv("trade.csv", index=False)


iclient = IndexerClient(config=config)


def pydantic_wrapper(model: Type[BaseModel]):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            if isinstance(result, list):
                return [model.model_validate(item) for item in result]
            else:
                return model.model_validate(result)

        return wrapper

    return decorator


@pydantic_wrapper(Transfer)
def get_transfers() -> List[Transfer]:
    return iclient.account.get_subaccount_transfers(
        address=PUBADDRES,
        subaccount_number=SUBACCOUNT,
        created_before_or_at_time=TO_DATE,
    ).data["transfers"]


@pydantic_wrapper(Position)
def get_positions() -> List[Position]:
    return iclient.account.get_subaccount_perpetual_positions(
        address=PUBADDRES,
        subaccount_number=SUBACCOUNT,
        created_before_or_at_time=TO_DATE,
    ).data["positions"]


@pydantic_wrapper(Order)
def get_orders() -> List[Order]:
    responses: list[dict] = []
    init_response = iclient.account.get_subaccount_orders(
        address=PUBADDRES, subaccount_number=SUBACCOUNT
    ).data
    ord_df = pd.DataFrame(init_response)
    ord_df["updatedAt"] = pd.to_datetime(ord_df["updatedAt"])
    max_date = pd.to_datetime(TO_DATE)
    # compare latest date in df to max date
    if ord_df["updatedAt"].max() < max_date:
        max_date = ord_df["updatedAt"].max()
        print("max date updated: ", max_date)
    ord_df["goodTilBlock"] = pd.to_numeric(ord_df["goodTilBlock"])
    good_til_block = ord_df["goodTilBlock"].min()
    # find if df contains max date
    ord_df["date"] = ord_df["updatedAt"].dt.date
    dates = ord_df["date"].unique().tolist()
    dates = [pd.to_datetime(date, errors="coerce").date()  for date in dates if not pd.isna(date)]
    while max_date.date() not in dates:
        response = iclient.account.get_subaccount_orders(
            address=PUBADDRES, subaccount_number=SUBACCOUNT, good_til_block_before_or_at=int(good_til_block)
        ).data
        ord_df = pd.DataFrame(response)
        ord_df["updatedAt"] = pd.to_datetime(ord_df["updatedAt"])
        ord_df["date"] = ord_df["updatedAt"].dt.date
        ord_df["goodTilBlock"] = pd.to_numeric(ord_df["goodTilBlock"])
        good_til_block = ord_df["goodTilBlock"].min()
        dates = ord_df["date"].unique().tolist()
        dates = [pd.to_datetime(date).date() for date in dates]
        print(f"{max_date in dates} -> {max_date} || {dates}")
        print(f"fetching backward orders with goodTilBlock: {good_til_block} maxdate response of this fetch: {max(dates)}")
        time.sleep(0.5)
    greatest_block = ord_df[ord_df["date"] == max_date.date()]["goodTilBlock"].max()
    
    print("found max block: ", greatest_block)
    top_response = iclient.account.get_subaccount_orders(
         address=PUBADDRES, subaccount_number=SUBACCOUNT, good_til_block_before_or_at=greatest_block
    ).data
    responses.extend(top_response)
    top_df = pd.DataFrame(top_response)
    top_df["goodTilBlock"] = pd.to_numeric(top_df["goodTilBlock"])
    min_top_block = top_df["goodTilBlock"].min()
    min_date = pd.to_datetime(FROM_DATE)
    print(len(top_response))
    while len(top_response) == LIMIT:
        top_response = iclient.account.get_subaccount_orders(
            address=PUBADDRES, subaccount_number=SUBACCOUNT, good_til_block_before_or_at=min_top_block
        ).data
        top_df = pd.DataFrame(top_response)
        ord_df["goodTilBlock"] = pd.to_numeric(ord_df["goodTilBlock"])
        min_top_block = top_df["goodTilBlock"].min()
        # check if there is a date that is less than min date
        top_df["updatedAt"] = pd.to_datetime(top_df["updatedAt"])
        dates = top_df["updatedAt"].dt.date.unique().tolist()
        dates = [pd.to_datetime(date).date() for date in dates]
        for date in dates:
            if date < min_date.date():
                print("min date reached:")
                top_df = top_df.loc[top_df["updatedAt"].dt.date == min_date.date()]
                top_df["updatedAt"] = top_df["updatedAt"].dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
                top_response = top_df.to_dict(orient="records")
                responses.extend(top_response)
                break
        responses.extend(top_response)
        time.sleep(0.5)
        print("min block fetched: ", min_top_block)
    return responses
    


@pydantic_wrapper(Trade)
def get_fills() -> List[Trade]:
    responses: list[dict] = []
    todate_plus = pd.to_datetime(TO_DATE) + pd.Timedelta("1d")
    todate_plus = todate_plus.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    response = iclient.account.get_subaccount_fills(
        address=PUBADDRES,
        subaccount_number=SUBACCOUNT,
        created_before_or_at_time=todate_plus,
    ).data["fills"]
    responses.extend(response)
    from_date = pd.to_datetime(FROM_DATE)
    while len(response) == LIMIT:
        df = pd.DataFrame(response)
        min_date = pd.to_datetime(df["createdAt"]).min().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        print(f"fetching more trades {min_date=}")
        response = iclient.account.get_subaccount_fills(
            address=PUBADDRES,
            subaccount_number=SUBACCOUNT,
            created_before_or_at_time=min_date,
        ).data["fills"]
        dates = pd.DataFrame(response)["createdAt"].unique().tolist()
        dates = [pd.to_datetime(date).date() for date in dates]
        for date in dates:
            if date < from_date.date():
                print("min date reached")
                df = pd.DataFrame(response)
                df["created_at_dt"] = pd.to_datetime(df["createdAt"])
                df = df.loc[df["created_at_dt"] >= from_date]
                response = df.to_dict(orient="records")
                break
        responses.extend(response)
        time.sleep(0.5)
    return responses
    
    


transfer_data = get_transfers()
position_data = get_positions()
order_data = get_orders()
fills_data = get_fills()

print(len(transfer_data))
print(len(position_data))
print(len(fills_data))

account_data = account(
    order=order_data,
    transfer=transfer_data, 
    position=position_data,
    trade=fills_data
)
account_data.create_csv()
